<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis(){
        return view ('register');
    }

    public function welcome(Request $form){
        $nama_d = $form["nama_depan"];
        $nama_b =$form["nama_belakang"];
        
        return view ('welcome', ["nama_d"=>$nama_d, "nama_b"=>$nama_b]);
    }
}
