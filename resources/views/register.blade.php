<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrasi</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>Firs Name :</label><br><br>
        <input type="text" name="nama_depan"><br><br>
        <label>Last Name :</label><br><br>
        <input type="text" name="nama_belakang"><br><br>
        <label>Gender :</label><br><br>
        <input type="radio" name="M" id="mal">Male<br>
        <input type="radio" name="F" id="fem">Female<br>
        <input type="radio" name="O" id="oth">Other<br><br>
        <label>Nationality :</label><br><br>
        <select name="Nas" id="nas">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapura">Singapore</option>
            <option value="Inggris">United Kingdom</option>
        </select><br><br>
        <label>Language Spoken :</label><br><br>
        <input type="checkbox" name="ina" id="ind">Bahasa Indonesia<br>
        <input type="checkbox" name="eng" id="ing">English<br>
        <input type="checkbox" name="oth" id="lain">Other<br><br>
        <label>Bio :</label><br><br>
        <textarea name="Bio" id="Bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>